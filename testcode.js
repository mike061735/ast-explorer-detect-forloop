let acorn = require("acorn");
const fs = require('fs');
let data = fs.readFileSync('test.txt').toString();
let res = acorn.parse(data)
//console.log(res)

function CodeException(message){
    this.message = message
    this.name = "CodeException"
}

CodeException.prototype.toString = function() {
    return this.name + ': "' + this.message + '"';
}


let traverseList = []

function traverse(res){
    for(let i in res){
        typeof res[i] == "object" ? traverse(res[i]) : traverseList.push(res[i])
    }
}


function ForstatmentCheck(arr){
    let cnt = 0;
    for(const item of arr){
        if(['ForOfStatement','ForStatement'].includes(item)){
             cnt++;
        }
    }

    if(cnt > 0){
        console.log("For 迴圈數量 => " + cnt);
        throw new CodeException("Code error. Contain For statement.")
    }else{
        console.log("ForstatmentCheck Test Pass!")
    }
}

function SortMapCheck(arr){
    let cnt = 0;
    for(const item of arr){
        if(['sort','filter','map'].includes(item)){
            cnt++
        }
    }

    if(cnt > 0){
        console.log("sort or map數量 => " + cnt);
        throw new CodeException("Code error. Contain sort or map function.")
    }else{
        console.log("SortMapCheck Test Pass!")
    }
}

let traverseFunc = [] 
function traverseFunction(res){//用來針對function區塊進行traverse
    for(let i in res){
        typeof res[i] == "object" ? traverseFunction(res[i]) : traverseFunc.push(res[i])
    }
}


function CallFunctionCheck(json, arr){
    let funcName = ""
    for(const item of json.body){
        if(["FunctionDeclaration"].includes(item.type)){
            traverseFunction(item)
            funcName = item.id.name
        }
    }

    let recursiveSize = -1
    for(const item of traverseFunc){
        if([funcName].includes(item)){
            recursiveSize++
        }
    }

    if (recursiveSize >= 1){
        console.log("Call function數量 => " + recursiveSize);
        console.log("Recursive Test Pass!")
    }else{
        console.log("Call function數量 => " + recursiveSize);
        throw new CodeException("Code error. No recursive.")
    }
}

traverse(res)
CallFunctionCheck(res, traverseList)
ForstatmentCheck(traverseList)
SortMapCheck(traverseList)
